<a href="#"><img width="100%" height="auto" src="https://i.imgur.com/iXuL1HG.png" height="175px"/></a>

<h1 align="center">Hi <img src="https://c.tenor.com/z2xJqhCpneIAAAAM/wave-hand.gif" width="30px">, I'm Balaji C</h1>


## 🙋‍♂️ About Me

- 🔭 I'm a passionate Data Scientist self-taught programmer from Chennai,India.

- 🌱 My Skills are Machine Learning | Deep Learning | NLP | OpenCV | WebTech

- 👯 I’m looking to collaborate on **OpenSource Projects**

- 📫 How to reach me **balajiavinash66@gmail.com**

- ⚡ Fun fact **I play games and go to the Shuttle very often.**

## 🚀 Languages and Tools:
<p align="left"> 
    <a href="https://www.python.org" target="_blank">  <img src="https://img.icons8.com/color/50/000000/python--v1.png"/></a>
    <a href="https://numpy.org" target="_blank">  <img src="https://user-images.githubusercontent.com/98330/63813335-20cd4b80-c8e2-11e9-9c04-e4dbf7285aa1.png" height=48 width=48/></a>
    <a href="https://pandas.pydata.org" target="_blank">  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Pandas_mark.svg/1200px-Pandas_mark.svg.png" height=48 width=48/></a>
    <a href="https://matplotlib.org" target="_blank">  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Created_with_Matplotlib-logo.svg/1024px-Created_with_Matplotlib-logo.svg.png" height=48 width=48/></a>
    <a href="https://seaborn.pydata.org" target="_blank">  <img src="https://user-images.githubusercontent.com/315810/92161415-9e357100-edfe-11ea-917d-f9e33fd60741.png" height=48 width=48/></a>
    <a href="https://plotly.com" target="_blank">  <img src="https://avatars.githubusercontent.com/u/5997976?s=200&v=4" height=48 width=48/></a>
    <a href="https://scikit-learn.org/stable" target="_blank">  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Scikit_learn_logo_small.svg/2560px-Scikit_learn_logo_small.svg.png" height=48 width=48/></a>
    <a href="https://keras.io" target="_blank">  <img src="https://keras.io/img/logo.png" height=48 width=48/></a>
     <a href="https://www.tensorflow.org" target="_blank">  <img src="https://www.tensorflow.org/resources/images/tf-logo-card-16x9.png" height=48 width=48/></a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://img.icons8.com/color/48/000000/javascript.png"/> </a> 
    <a href="https://www.w3.org/html" target="_blank"> <img src="https://img.icons8.com/color/48/000000/html-5.png"/> </a> 
    <a href="https://www.w3schools.com/css" target="_blank"> <img src="https://img.icons8.com/color/48/000000/css3.png"/> </a> 
    <a href="https://getbootstrap.com" target="_blank"> <img src="https://img.icons8.com/color/48/000000/bootstrap.png"/> </a> 
    <a style="padding-right:8px;" href="https://www.mysql.com/" target="_blank"> <img src="https://img.icons8.com/fluent/50/000000/mysql-logo.png"/> </a>
    <a href="https://www.mongodb.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="48" height="48"/> </a> 
    <a href="https://www.djangoproject.com" target="_blank">  <img src="https://i.pinimg.com/originals/d9/70/36/d97036b366cf7dc60cebf3a4fee58463.png" height=48 width=48/></a>
    <a href="https://flask.palletsprojects.com/en/2.0.x" target="_blank">  <img src="https://miro.medium.com/max/438/1*0G5zu7CnXdMT9pGbYUTQLQ.png" height=48 width=48/></a>
    <a href="https://docs.opencv.org/4.x/d6/d00/tutorial_py_root.html" target="_blank">  <img src="https://editor.analyticsvidhya.com/uploads/232202.png" height=48 width=48/></a>

    
</p>

<!-- [![React Badge](https://img.shields.io/badge/-React-61DBFB?style=for-the-badge&labelColor=black&logo=react&logoColor=61DBFB)](#)  [![Javascript Badge](https://img.shields.io/badge/-Javascript-F0DB4F?style=for-the-badge&labelColor=black&logo=javascript&logoColor=F0DB4F)](#) [![Typescript Badge](https://img.shields.io/badge/-Typescript-007acc?style=for-the-badge&labelColor=black&logo=typescript&logoColor=007acc)](#) [![Nodejs Badge](https://img.shields.io/badge/-Nodejs-3C873A?style=for-the-badge&labelColor=black&logo=node.js&logoColor=3C873A)](#) [![GraphQL Badge](https://img.shields.io/badge/-GraphQl-e535ab?style=for-the-badge&labelColor=black&logo=node.js&logoColor=e535ab)](#) -->
<br/>

<p align="center">
    <a href="https://github.com/Balajichandra/github-readme-streak-stats">
        <img title="🔥 Get streak stats for your profile at git.io/streak-stats" alt="Balaji streak" src="https://github-readme-streak-stats.herokuapp.com/?user=Balajichandra&theme=black-ice&hide_border=true&stroke=0000&background=060A0CD0"/>
    </a>
</p>

## 📊 My Github Stats

  <br/>
    <a href="https://github.com/Balajichandra/github-readme-stats"><img alt="Balaji Github Stats" src="https://github-readme-stats.vercel.app/api?username=Balajichandra&show_icons=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117" /></a>
  <a href="https://github.com/Balajichandra/github-readme-stats"><img alt="Balaji Top Languages" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Balajichandra&langs_count=8&count_private=true&layout=compact&theme=react&hide_border=true&bg_color=0D1117" /></a>
  <br/>
  <b>Note:</b> Top languages is only a metric of the languages my public code consists of and doesn't reflect experience or skill level.


<br/>
<br/>

<a href="https://github.com/Balajichandra/github-readme-activity-graph"><img alt="Balaji Activity Graph" src="https://activity-graph.herokuapp.com/graph?username=Balajichandra&bg_color=0D1117&color=5BCDEC&line=5BCDEC&point=FFFFFF&hide_border=true" /></a>

<br/>
<br/>

## Connect with me:
<p align="left">

<a href = "https://www.linkedin.com/in/balajiavinash/"><img src="https://img.icons8.com/fluent/48/000000/linkedin.png"/></a>

</p>

## ❤ Views and Followers
<a href="https://github.com/Meghna-DAS/github-profile-views-counter">
    <img src="https://komarev.com/ghpvc/?username=Balajichandra">
</a>
<a href="https://github.com/Balajichandra?tab=followers"><img src="https://img.shields.io/github/followers/Balajichandra?label=Followers&style=social" alt="GitHub Badge"></a>
